import { DOTAGameUIState_t, DOTA_GameMode, DOTA_GameState, GameRules, GameState } from "wrapper/Imports"
import { RolesState } from "../menu"

export class ROLES_VALIDATE {
	public static get IsPreGame() {
		return RolesState.value
			&& GameRules !== undefined
			&& GameState.UIState === DOTAGameUIState_t.DOTA_GAME_UI_DOTA_INGAME
			&& (
				GameRules.GameState === DOTA_GameState.DOTA_GAMERULES_STATE_HERO_SELECTION
				|| GameRules.GameState === DOTA_GameState.DOTA_GAMERULES_STATE_STRATEGY_TIME
				|| GameRules.GameState === DOTA_GameState.DOTA_GAMERULES_STATE_WAIT_FOR_MAP_TO_LOAD
				|| GameRules.GameState === DOTA_GameState.DOTA_GAMERULES_STATE_WAIT_FOR_PLAYERS_TO_LOAD
			)
			&& (
				GameRules.GameMode === DOTA_GameMode.DOTA_GAMEMODE_ALL_DRAFT
				|| GameRules.GameMode === DOTA_GameMode.DOTA_GAMEMODE_RD
				|| GameRules.GameMode === DOTA_GameMode.DOTA_GAMEMODE_CM
			)
	}
}
