import { EventsX } from "../../../X-Core/Imports"
import { HERO_SELECTION } from "../../data"

EventsX.on("GameEnded", () => {
	HERO_SELECTION.Dispose()
})
