import { DOTA_ABILITY_BEHAVIOR, DOTA_RUNES, EntityManager, EventsSDK, GameSleeper, GameState, Hero, Input, item_bottle, LocalPlayer, MathSDK, Menu as MenuSDK } from "wrapper/Imports"

const Menu = MenuSDK.AddEntryDeep(["Utility", "Illusion Splitter"])
const AngleRandomizer = Menu.AddToggle("Random Split Angle", true)
const IllusionsRange = Menu.AddSlider("Illusions Range", 600, 100, 2000)
const IllusionsMinMoveRange = Menu.AddSlider("Minimun Move Range", 800, 100, 2000)
const MoveMainHero = Menu.AddToggle("Move Hero", true)
const Hotkey = Menu.AddKeybind("Hotkey")

const arr_abil: string[] = [
	"item_bottle_illusion",
	"item_manta",
	"item_illusionsts_cape",
	"naga_siren_mirror_image",
	"terrorblade_conjure_image",
	"phantom_lancer_doppelwalk",
]
const AbilityMenu = Menu.AddImageSelector("Usage", arr_abil, new Map(arr_abil.map(name => [name, true])))

const Sleep = new GameSleeper()
const Delay = () => (((GameState.Ping / 2) + 30) + 250)

EventsSDK.on("Tick", () => {

	if (LocalPlayer!.Hero === undefined || !Hotkey.is_pressed)
		return

	const Owner = LocalPlayer!.Hero

	if (Sleep.Sleeping(Owner))
		return

	const illusions = EntityManager.GetEntitiesByClass(Hero).filter(x =>
		x.IsIllusion
		&& x.IsAlive
		&& x.IsControllable
		&& !x.UnitState.some(z => z & 524288) // Anti mage restrict talant
		&& (x.Distance2D(Owner) < IllusionsRange.value),
	)

	const unitCount = illusions.length + 1
	const angleUnit = 360 / unitCount

	if (MoveMainHero.value && !Sleep.Sleeping("owner_move")) {
		Owner.MoveTo(Input.CursorOnWorld)
		Sleep.Sleep(Delay(), "owner_move")
	}

	let Direction = MoveMainHero.value
		? Input.CursorOnWorld.SubtractForThis(Owner.Position)
		: Owner.InFront(250).SubtractForThis(Owner.Position)

	const midPosition = illusions.reduce((current, illusion) =>
		illusion.Position.Add(current), Owner.Position)

	midPosition.DivideScalarForThis(unitCount)

	illusions.forEach(illusion => {

		const randomAngle = Math.floor(Math.random() * Math.floor(angleUnit / unitCount) + 1)

		Direction = AngleRandomizer.value
			? Direction.Rotated(MathSDK.DegreesToRadian(angleUnit + randomAngle))
			: Direction.Rotated(MathSDK.DegreesToRadian(angleUnit))

		const movePos = midPosition.Add(
			Direction.Normalize().MultiplyScalarForThis(IllusionsMinMoveRange.value),
		)

		if (Sleep.Sleeping(illusion))
			return

		illusion.MoveTo(movePos)
		Sleep.Sleep(Delay(), illusion)
		return
	})

	if (Owner.IsInvulnerable || Owner.IsStunned)
		return

	// ###### usage ability
	if (!arr_abil.some(x => {

		const bottle = Owner.GetItemByName("item_bottle"),
			abil = Owner.GetAbilityByName(x) ?? Owner.GetItemByName(x)

		if (
			bottle !== undefined
			&& bottle.CanBeCasted()
			&& AbilityMenu.IsEnabled("item_bottle_illusion")
		) {
			const bottleStored = bottle as item_bottle
			if (bottleStored.StoredRune === DOTA_RUNES.DOTA_RUNE_ILLUSION) {
				Owner.CastNoTarget(bottle)
				Sleep.Sleep(Delay(), Owner)
				return true
			}
		}

		if (
			abil === undefined
			|| !abil.CanBeCasted()
			|| !AbilityMenu.IsEnabled(x)
		)
			return false

		const delay_defualt = !x.startsWith("item_")
			? abil.CastPoint * 1000 + Delay()
			: Delay()

		if (abil.HasBehavior(DOTA_ABILITY_BEHAVIOR.DOTA_ABILITY_BEHAVIOR_POINT)) {
			const pos = Input.CursorOnWorld.Subtract(Owner.Position)
			if (pos.Length > abil.CastRange)
				pos.Normalize().MultiplyScalarForThis(abil.CastRange)

			Owner.CastPosition(abil, Owner.Position.Add(pos))
			Sleep.Sleep((abil.CastPoint + abil.ActivationDelay) * 1000 + Delay(), Owner)
			return true
		}

		if (x.startsWith("naga_")) {
			Owner.CastNoTarget(abil)
			Sleep.Sleep((abil.CastPoint + abil.ActivationDelay) * 1000 + Delay(), Owner)
			return true
		}

		Owner.CastNoTarget(abil)
		Sleep.Sleep(delay_defualt, Owner)
		return true

	})) return
})
