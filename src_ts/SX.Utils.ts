import { LocationX } from "gitlab.com/octarine-private/immortal-core/Imports"
import { Color, DotaMap, EntityManager, EventsSDK, Hero, LocalPlayer, npc_dota_hero_antimage, npc_dota_hero_lion, RendererSDK, TickSleeper } from "./wrapper/Imports"

EventsSDK.on("Draw", () => {
	const player = LocalPlayer?.Hero
	if (!(globalThis as any).DOTA_MAP || player === undefined)
		return

	const w2s = RendererSDK.WorldToScreen(player.Position)
	if (w2s === undefined)
		return
	const area = DotaMap.GetMapArea(player.Position)
	const area_name = LocationX.MapAreaLocationStr(area)
	RendererSDK.Text(
		area_name.str_position_name + area_name.str_position,
		w2s,
		Color.LightGray,
	)
});

(globalThis as any).AM_TEST = false
const Sleeper = new TickSleeper()
let netPos = 0
EventsSDK.on("Tick", () => {
	if (!(globalThis as any).AM_TEST)
		return
	if (LocalPlayer === undefined || Sleeper.Sleeping)
		return

	const hero = LocalPlayer.Hero
	if (!(hero instanceof npc_dota_hero_antimage))
		return

	// const enemyAbil = EntityManager.GetEntitiesByClass(Ability).filter(x => x.Owner?.IsEnemy()
	// 	&& x instanceof lion_voodoo
	// 	&& x.CanBeCasted()
	// 	&& x.CanHit(hero)
	// )[0]

	const enemy = EntityManager.GetEntitiesByClass(Hero).filter(x => x instanceof npc_dota_hero_lion && x.Distance(hero) <= 600)[0]

	if (enemy === undefined)
		return

	const abil = hero.Spells[2]
	netPos = enemy.FindRotationAngle(hero)

	if (abil === undefined || !abil.CanBeCasted())
		return

	if (netPos < 1.7) {
		hero.CastNoTarget(abil)
		Sleeper.Sleep(500)
	}
})

EventsSDK.on("Draw", () => {
	if (!(globalThis as any).AM_TEST)
		return

	if (LocalPlayer === undefined)
		return

	const hero = LocalPlayer.Hero
	if (!(hero instanceof npc_dota_hero_antimage))
		return

	const enemy = EntityManager.GetEntitiesByClass(Hero).filter(x => x instanceof npc_dota_hero_lion && x.Distance(hero) <= 1000)[0]

	if (enemy === undefined)
		return

	const abil = hero.Spells[2]
	if (abil === undefined)
		return

	const position = RendererSDK.WorldToScreen(hero.Position)
	if (position === undefined || position.IsZero())
		return

	RendererSDK.Text(`FindRotationAngle: ${netPos}`, position, Color.White, RendererSDK.DefaultFontName, 20)
})
