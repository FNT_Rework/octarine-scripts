// import { Color, Creep, DOTAGameUIState_t, Entity, EntityManager, EventsSDK, GameRules, GameSleeper, GameState, Hero, InfoPlayerStartDota, LocalPlayer, Menu, MovingObstacle, NavMeshPathfinding, Obstacle, PlayerResource, pudge_meat_hook, RendererSDK, Unit, Vector2, Vector3 } from "./wrapper/Imports"

// const menu = Menu.AddEntryDeep(["Utility", "Good Respawn"])
// const visuals_state = menu.AddToggle("Visuals State", true),
// 	debug_visuals_state = menu.AddToggle("Debug Visuals State", false),
// 	autohook_state = menu.AddToggle("AutoHook State", true),
// 	autohook_delay = menu.AddSlider("AutoHook Delay", 0, -6, 6),
// 	lock_position = menu.AddToggle("Lock position to 1 hook", false),
// 	manual_fix = menu.AddSlider("Manual position fix", 0, 0, 14)

// function GetPositions(): Vector3[] {
// 	const local_team = LocalPlayer?.Team
// 	return EntityManager.GetEntitiesByClass(InfoPlayerStartDota).filter(e => e.SpawnerTeam !== local_team).map(a => a.Position)
// }

// EventsSDK.on("Draw", () => {
// 	if (!visuals_state.value || GameState.UIState !== DOTAGameUIState_t.DOTA_GAME_UI_DOTA_INGAME || PlayerResource === undefined)
// 		return
// 	const positions = GetPositions(),
// 		next_spawn = GetNextSpawn()
// 	if (debug_visuals_state.value) {
// 		positions.forEach((e, i) => {
// 			const screen_pos = RendererSDK.WorldToScreen(e)
// 			if (screen_pos === undefined)
// 				return
// 			const text = i.toString()
// 			const text_size = Vector2.FromVector3(RendererSDK.GetTextSize(text))
// 			const box_bounds = text_size.Clone().AddScalar(4)
// 			screen_pos.SubtractForThis(box_bounds.DivideScalar(2))
// 			RendererSDK.FilledRect(screen_pos, box_bounds, Color.Red)
// 			RendererSDK.Text(text, new Vector2(screen_pos.x + box_bounds.x / 2 - text_size.x / 2, screen_pos.y + box_bounds.y - box_bounds.y / 2 + text_size.y / 2), Color.Green)
// 		})
// 	}
// 	const ar: number[] = []
// 	for (let i = 0; i < PlayerResource.PlayerTeamData.length; i++) {
// 		const team_data = PlayerResource.PlayerTeamData[i]
// 		if (PlayerResource.PlayerData[i].Team === LocalPlayer?.Team)
// 			continue

// 		const ent = EntityManager.EntityByIndex(team_data.SelectedHeroIndex)
// 		if (!(ent instanceof Hero))
// 			continue

// 		let respawn_time = ent.RespawnTime - GameRules!.RawGameTime
// 		if (respawn_time <= 0)
// 			respawn_time = team_data.RespawnSeconds
// 		if (respawn_time <= 0)
// 			continue

// 		ar.push(respawn_time)
// 	}
// 	ar.sort((a, b) => a - b).forEach((respawn_time, i) => {
// 		const pos = positions[positions.length !== 0 ? (next_spawn - (ar.length - i - 1)) % positions.length : 0]
// 		if (pos === undefined)
// 			return
// 		const screen_pos = RendererSDK.WorldToScreen(pos)
// 		if (screen_pos === undefined)
// 			return
// 		const text = respawn_time.toFixed(1)
// 		const text_size = Vector2.FromVector3(RendererSDK.GetTextSize(text))
// 		const box_bounds = text_size.Clone().AddScalar(4)
// 		screen_pos.SubtractForThis(box_bounds.DivideScalar(2))
// 		RendererSDK.FilledRect(screen_pos, box_bounds, Color.Green)
// 		RendererSDK.Text(text, new Vector2(screen_pos.x + box_bounds.x / 2 - text_size.x / 2, screen_pos.y + box_bounds.y - box_bounds.y / 2 + text_size.y / 2), Color.Red)
// 	})
// })

// function GetEnemyDeaths() {
// 	let deaths = 0
// 	for (let i = 0; i < PlayerResource!.PlayerData.length; i++) {
// 		const team_data = PlayerResource!.PlayerTeamData[i]
// 		if (team_data !== undefined && PlayerResource!.PlayerData[i].Team !== LocalPlayer?.Team)
// 			deaths += team_data.Deaths
// 	}
// 	return deaths
// }
// function GetNextSpawn() {
// 	return manual_fix.value + 4 + PlayerResource!.PlayerData.filter(data => data.Team !== LocalPlayer?.Team).length + GetEnemyDeaths()
// }

// const hook_sleeper = new GameSleeper()
// EventsSDK.on("Tick", () => {
// 	if (!autohook_state.value || PlayerResource === undefined)
// 		return
// 	const positions = GetPositions(),
// 		next_spawn = GetNextSpawn()
// 	const ar: [number, Hero][] = []
// 	for (let i = 0; i < PlayerResource.PlayerTeamData.length; i++) {
// 		const team_data = PlayerResource.PlayerTeamData[i]
// 		if (PlayerResource.PlayerData[i].Team === LocalPlayer?.Team)
// 			continue

// 		const ent = EntityManager.EntityByIndex(team_data.SelectedHeroIndex)
// 		if (!(ent instanceof Hero))
// 			continue

// 		let respawn_time = ent.RespawnTime - GameRules!.RawGameTime
// 		if (respawn_time <= 0)
// 			respawn_time = team_data.RespawnSeconds
// 		if (respawn_time <= 0)
// 			continue

// 		ar.push([respawn_time, ent])
// 	}
// 	ar.sort(([a], [b]) => a - b).forEach(([respawn_time, target], i) => {
// 		if (respawn_time > 4)
// 			return
// 		const pos = positions[positions.length !== 0 ? (next_spawn - (ar.length - i - 1)) % positions.length : 0]
// 		if (pos === undefined || hook_sleeper.Sleeping(pos.LengthSqr))
// 			return
// 		EntityManager.GetEntitiesByClass(Unit).some(unit => {
// 			if (!unit.IsAlive || !unit.IsControllable || unit.IsEnemy() || unit.IsInvulnerable || hook_sleeper.Sleeping(unit) || unit.IsInAbilityPhase)
// 				return false

// 			const hook = unit.GetAbilityByClass(pudge_meat_hook)
// 			if (hook === undefined || !hook.IsCooldownReady || !unit.IsInRange(pos, hook.CastRange + hook.AOERadius))
// 				return false

// 			const obs2ent = new Map<Obstacle, Entity>(),
// 				start_pos = Vector2.FromVector3(unit.Position),
// 				target_obs = new Obstacle(Vector2.FromVector3(pos), target.HullRadius)
// 			const ents = [...EntityManager.GetEntitiesByClass(Creep), ...EntityManager.GetEntitiesByClass(Hero)]
// 			ents.forEach(ent => {
// 				if (ent !== unit && ((ent.IsAlive && ent.IsInRange(unit, hook.CastRange * 2) && !ent.IsInvulnerable && ent.IsVisible && !ent.IsEnemy()) || ent === target))
// 					obs2ent.set(
// 						ent !== target
// 							? MovingObstacle.FromUnit(ent)
// 							: target_obs,
// 						ent,
// 					)
// 			})
// 			const predict_res = new NavMeshPathfinding(
// 				new MovingObstacle(
// 					start_pos/*.Add(unit.Forward.toVector2().MultiplyScalarForThis(hook.AOERadius * 1.5))*/,
// 					hook.AOERadius,
// 					Vector2.FromVector3(unit.Position.GetDirectionTo(pos)).MultiplyScalarForThis(hook.Speed),
// 					hook.CastRange / hook.Speed,
// 				),
// 				[...obs2ent.keys()],
// 				hook.CastPoint + unit.TurnTime(pos) + (autohook_delay.value / 60) + (GameState.Ping / 2000),
// 			).GetFirstHitObstacle()
// 			if (predict_res === undefined || predict_res[0] !== target_obs || respawn_time > predict_res[1] - (GameState.Ping / 2000))
// 				return false
// 			unit.CastPosition(hook, unit.Position.Extend(pos, hook.CastRange / 1.5))
// 			hook_sleeper.Sleep((GameState.Ping / 2) + (hook.CastPoint * 1000), unit)
// 			if (lock_position.value)
// 				hook_sleeper.Sleep((GameState.Ping / 2) + (hook.CastPoint * 1000), pos.LengthSqr)
// 			return true
// 		})
// 	})
// })

// EventsSDK.on("PrepareUnitOrders", args => !hook_sleeper.Sleeping(args.Issuers[0]))
// EventsSDK.on("GameEnded", () => hook_sleeper.FullReset())
